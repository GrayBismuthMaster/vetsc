import { Route } from '@angular/router';
import { authGuard } from 'dashboard/core/authentication/auth.guard';

export const appRoutes: Route[] = [
  {

    path: '',

    loadComponent: () => import('dashboard/theme/admin-layout/admin-layout.component').then(m => m.AdminLayoutComponent),
    canActivate: [authGuard],
    canActivateChild: [authGuard],
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', loadComponent: () => import('dashboard/routes/dashboard/dashboard.component').then(m => m.DashboardComponent),},
      { path: '403', loadComponent: () => import('dashboard/routes/sessions/403.component').then(m => m.Error403Component),},
      { path: '404', loadComponent: () => import('dashboard/routes/sessions/404.component').then(m => m.Error404Component),},
      { path: '500', loadComponent: () => import('dashboard/routes/sessions/500.component').then(m => m.Error500Component), },
      {
        path: 'design',
        loadChildren: () => import('../../../dashboard/src/app/routes/design/design.routes').then(m => m.routes),
      },
      {
        path: 'material',
        loadChildren: () => import('../../../dashboard/src/app/routes/material/material.routes').then(m => m.routes),
      },
      {
        path: 'media',
        loadChildren: () => import('../../../dashboard/src/app/routes/media/media.routes').then(m => m.routes),
      },
      {
        path: 'forms',
        loadChildren: () => import('../../../dashboard/src/app/routes/forms/forms.routes').then(m => m.routes),
      },
      {
        path: 'tables',
        loadChildren: () => import('../../../dashboard/src/app/routes/tables/tables.routes').then(m => m.routes),
      },
      {
        path: 'profile',
        loadChildren: () => import('../../../dashboard/src/app/routes/profile/profile.routes').then(m => m.routes),
      },
      {
        path: 'permissions',
        loadChildren: () => import('../../../dashboard/src/app/routes/permissions/permissions.routes').then(m => m.routes),
      },
      {
        path: 'utilities',
        loadChildren: () => import('../../../dashboard/src/app/routes/utilities/utilities.routes').then(m => m.routes),
      },
    ],
  },
  {
    path: 'auth',
    loadComponent: () => import('dashboard/theme/auth-layout/auth-layout.component').then(m => m.AuthLayoutComponent),
    children: [
      {  path: 'login', loadComponent: () => import('dashboard/routes/sessions/login/login.component').then(m => m.LoginComponent)},
      { path: 'register', loadComponent: () => import('dashboard/routes/sessions/register/register.component').then(m => m.RegisterComponent)},
    ],
  },
  { path: '**', redirectTo: 'dashboard' },
];
